class Libreria3 {
    constructor() {
    }

    metodo1() {
        console.log('Soy el metodo 1 de la libreria 3');
    }

    /**
     * Metodo 2
     */
    metodo2(parametro = 'una bosta') {
        console.log('Soy el metodo 2 de la libreria 3');
        console.log(`Este cursito es ${parametro}`);
    }
}

export { Libreria3 }