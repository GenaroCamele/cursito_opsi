/**
 * Funcion que imprime un numero
 * NOTA: defino tipo de parametro y tipo de retorno
 */
function imprimirEjemplo(numero: number): boolean {
    console.log('Numero -> ', numero);
    return true;
}


// imprimirEjemplo(true); // Falla en compilacion (tipo de parametro incorrecto)
imprimirEjemplo(4); // No falla

// const a: number = imprimirEjemplo(true); // Falla en compilacion (tipo de variable de retorno incorrecto)
const a: boolean = imprimirEjemplo(4); // No falla

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
// Puedo definir interfaces
interface Persona {
    nombre: string;
    apellido: string;
    edad?: number; // Con el signo de pregunta significa que el atributo es opcional
}

function imprimirObjeto(persona: Persona): void {
    console.log('Persona -> ', persona);
}

// Ambos ejemplos de abajo funcionan
imprimirObjeto({
    nombre: 'Nicky',
    apellido: 'Malcorra'
});

imprimirObjeto({
    nombre: 'Nicky',
    apellido: 'Malcorra',
    edad: 24
});

// const per: Persona = { nombre: 'Prueba' } // Falla porque falta el parametro 'apellido'
const per2: Persona = {
    nombre: 'Prueba',
    apellido: 'Pruebita'
}

imprimirObjeto(per2);

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
// Puedo crear ENUMS
enum TipoDeError {
    FALTAN_DATOS = 1,
    AFILIADO_INCORRECTO = 2,
    OTRO = 3
}

let error: number = 2;
switch (error) {
    case TipoDeError.FALTAN_DATOS:
        console.log('Faltan datos!!!');
        break;
    case TipoDeError.AFILIADO_INCORRECTO:
        console.log('El afiliado es incorrecto!!!');
        break;
    case TipoDeError.OTRO:
        console.log('Otro error ocurrio!!!');
        break;
    default:
        console.log('Error desconocido');
        break;
}