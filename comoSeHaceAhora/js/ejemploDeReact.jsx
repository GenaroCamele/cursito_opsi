// Importo los componentes props hechos en React
import AppPrincipal from './componentesReact/appPrincipal.jsx'

// Importo ReactDOM para renderizar en el HTML
import React from 'react';
import ReactDOM from "react-dom";

ReactDOM.render(
    <AppPrincipal/>,
    document.getElementById('app-de-react')
);