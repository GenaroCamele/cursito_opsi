import React from 'react';

/**
 * Componente en formato de funcion, es mas liviano que una clase
 * pero no puede tener funcionamiento propio
 * @param {any} props Props del componente
 */
export default function EjemploDeComponenteConFuncion(props) {
    if (props.cantidadDeNumeros > 5) {
        return <h2>Hay más de 5 números!</h2>;
    }
    
    return <h2>Hay menos de 5 números!</h2>;
}