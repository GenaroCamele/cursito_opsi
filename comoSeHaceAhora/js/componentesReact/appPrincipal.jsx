import React from 'react';

// Otros componentes
import EjemploDeComponenteConFuncion from './EjemploDeComponenteConFuncion.jsx';

/**
 * Ejemplo de react
 * Exporto como default para importar en otros archivos como quiera
 */
export default class App extends React.Component {
    constructor(props) {
        super(props); // Esta linea es obligatoria para React

        this.state = {
            tiempo: new Date(), // Tiempo actual
            listaNumeros: [], // Lista de numeros
            nombre: 'Adalberto'
        };
        
        // Todos los metodos que se referencian en el HTML (NO SE LLAMAN DIRECTAMENTE)
        // debe tener bindeado la variable 'this'
        this.agregarNumero = this.agregarNumero.bind(this);
    }

    /**
     * Este metodo se ejecuta cada vez que se termina de montar el 
     * componente react
     */
    componentDidMount() {
        this.idIntervalo = setInterval(() => {
            this.actualizarTiempo()
        }, 1000);
    }

    /**
     * Actualiza el estado para que refresque la vista con el tiempo actual
     */
    actualizarTiempo() {
        this.setState({ tiempo: new Date() });
    }

    /**
     * Agrega un numero random a la lista de numeros
     */
    agregarNumero() {
        const listaNumeros = this.state.listaNumeros;
        listaNumeros.push(Math.random());
        this.setState({listaNumeros}); // Puedo especificar solo la clave y se actualiza
    }

    render() {
        // Puedo usar javascript como quiera!
        const tiempoEnString = this.state.tiempo.toLocaleTimeString();

        // Genero un <li> por cada numero de mi lista
        const listaDeNumeros = this.state.listaNumeros.map((numero) => {
            // NOTA: Todos los elementos de un map deben tener un key para que React funcione optimamente
            return <li key={numero.toString()}>{numero}</li>;
        });

        // Devuelvo el contenido HTML que queremos renderizar
        return (
            <div>
                {/* Tiempo actual */}
                <h1>Tiempo actual: { tiempoEnString }</h1>

                {/* Input */}
                <input type="text" value={this.state.nombre}/>

                {/* Lista de numeros */}
                <h2>Lista de numeros</h2>
                <button onClick={this.agregarNumero}>Agregar numero</button>
                <ul>
                    {listaDeNumeros}
                </ul>

                {/* Componente importado */}
                <EjemploDeComponenteConFuncion
                    cantidadDeNumeros={this.state.listaNumeros.length}
                />
            </div>
        );
    }
}