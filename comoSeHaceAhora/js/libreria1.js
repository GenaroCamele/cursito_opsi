/**
 * Imprime mensaje de la libreria
 */
function imprimirLibreria1() {
    console.log('Soy la libreria 1');
}

export { imprimirLibreria1 } // Con este tipo de export (sin 'default') se debe importar con el nombre exacto