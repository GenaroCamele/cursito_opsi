// Debo importar las librerias
import {imprimirLibreria1} from './libreria1';

// Las que exporte como default puede ser importadas con cualquier nombre
import unaFuncionDeLaLibreria2 from './libreria2';
import {Libreria3} from './libreria3';

// Los estilos se importan desde el js asi Webpack lo puede incluir
// (NECESITA UN LOADER DE CSS)
// import '../css/estilos1.css';
// import '../css/estilos2.css';

// Uso la libreria 1
imprimirLibreria1();

// Uso la libreria 2
unaFuncionDeLaLibreria2();

// Uso la libreria 3
let libreria3 = new Libreria3();
libreria3.metodo1();
libreria3.metodo2('la hostia');