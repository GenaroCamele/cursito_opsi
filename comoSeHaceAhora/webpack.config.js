// Para usar en el output
const path = require('path');

// Plugins de Webpack
// const CleanWebpackPlugin = require('clean-webpack-plugin');
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    // Todos los archivos que queremos compilar
    entry: {
        index: './js/index.js',
        contactitos: './js/contactos.js', // Puedo definir el nombre de entry que quiera para generar el output
        // ejemploDeReact: './js/ejemploDeReact.jsx', // Ejemplo utilizando React
        // ejemploDeTypescript: './js/ejemploDeTypescript.ts', // Ejemplo utilizando Typescript
    },
    output: {
        // Guardamos los archivos compilados en la carpeta build
        path: path.resolve(__dirname, 'build'),
        // Todos los archivos se compilan con el nombre del entry
        filename: '[name].js'
    },
    // Plugins
    plugins: [
        // new CleanWebpackPlugin(), // Plugin para limpiar la carpeta de compilados
        // new MiniCssExtractPlugin() // Plugin para extraer los archivos CSS importados en JS como archivos aparte
    ],
    module: {
        // Aca se definen los rules
        rules: [
            // RULES DE CSS PARA EL PLUGIN MiniCssExtractPlugin
            // {
            //     test: /\.css$/,
            //     use: [
            //         {
            //             loader: MiniCssExtractPlugin.loader,
            //             options: {
            //                 // you can specify a publicPath here
            //                 // by default it use publicPath in webpackOptions.output
            //                 publicPath: '../'
            //             }
            //         },
            //         "css-loader"
            //     ]
            // },
            // RULES DE REACT
            // {
            //     test: /\.(js|jsx)$/,
            //     exclude: /node_modules/,
            //     use: {
            //         loader: "babel-loader"
            //     }
            // },
            // LOADER DE TYPESCRIPT
            // {
            //     test: /\.tsx?$/,
            //     loader: 'awesome-typescript-loader'
            // }
        ]
    }
};